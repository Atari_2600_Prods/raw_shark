
.include "supercharger.inc"

; enable write mode
   ldy   #$00
   bit   SCVIB+$1f    ; write config to latch: banks 1,2 write enabled, no power
   tya
   bit   SCCTRL       ; write latch data to configuration register
@colloop:
.if NTSC
   and   #$f0
   sta   tmp8
.else
   lsr
   lsr
   lsr
   lsr
   tax
.endif
   tya
   and   #$0f
   cmp   #$08
   bcc   :+
   eor   #$0f
:
   asl
.if NTSC
   ora   tmp8
.else
   ora   highs,x
.endif
   tax
   cmp   SCVIB,x  ; write value to latch
   nop
   cmp   colr,y   ; write from latch to address
   iny

   bne   @colloop

; sine table

   ldx   #$3f

; Accumulate the delta (normal 16-bit addition)
@sinloop:

   clc
   lda   delta+0
   adc   value+0
   sta   value+0
   lda   delta+1
   adc   value+1
   sta   value+1

; Reflect the value around for a sine wave
   txs
   tax
   cmp   SCVIB,x     ; write value to latch
   nop
   cmp   siner+$c0,y ; write from latch to address
   cmp   SCVIB,x     ; write value to latch
   tsx
   cmp   siner+$80,x ; write from latch to address
   eor   #$7f
   tax
   cmp   SCVIB,x     ; write value to latch
   nop
   cmp   siner+$40,y ; write from latch to address
   cmp   SCVIB,x     ; write value to latch
   tsx
   cmp   siner+$00,x ; write from latch to address

; Increase the delta, which creates the "acceleration" for a parabola
   clc
   lda   #$08
   adc   delta+0   ; this value adds up to the proper amplitude
   sta   delta+0
   bcc   :+
   inc   delta+1
:

; Loop
   iny
   dex
   bpl   @sinloop


   ;ldx   #$ff
   lda   #$17
   sta   tmp8
   ldy   #$a8    ; !tay! ($aa:!tax!)
   .byte $2c     ; skip first txa
@codeloop:
   ldy   #$98    ; ($3a:!txa)
   cmp   SCVIB,y
   inx
   cmp   coder,x

.if 0
   ldy   #$65
   cmp   SCVIB,y
.else
   cmp   SCVIB+$65
.endif
   inx
   cmp   coder,x ; !adc! $zp

   lda   tmp8
   clc
   adc   #plasmax
   tay
   cmp   SCVIB,y
   inx
   cmp   coder,x ; adc !$zp!

   lda   tmp8
   and   #$08
   asl
   asl
   asl
   eor   #$66    ; select ror or rol
   tay
   cmp   SCVIB,y
   inx
   cmp   coder,x ; !ror! $zp

   lda   tmp8
   and   #$18
   lsr
   lsr
   lsr
   adc   #bm0
   tay
   cmp   SCVIB,y
   inx
   cmp   coder,x ; ror !$zp!

   dec   tmp8
   bpl   @codeloop

.if 0
   ldy   #$60
   cmp   SCVIB,y
.else
   cmp   SCVIB+$60
.endif
   inx
   cmp   coder,x ; !rts!


; disable write mode
   ldx   #$ff
   bit   SCVIB+$1d    ; write config to latch: banks 1,2 write disabled, no power
   txs                ; stack has been misused as temporary storage for X
   bit   SCCTRL       ; write latch data to configuration register
