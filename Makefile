
OBJDIR	:= obj
DBGDIR	:= debug

include Makefile.config

all: $(DBGDIR)/$(PRJNAME)_pal_2k.bin $(DBGDIR)/$(PRJNAME)_ntsc_2k.bin
	$(LS) $(DBGDIR)/*.bin
	grep ^CODE $(DBGDIR)/*.map

release: menu/menu.bin supercharger/$(PRJNAME)_pal.bin supercharger/$(PRJNAME)_ntsc.bin all

menu/menu.bin:
	make -C menu

supercharger/$(PRJNAME)_pal.bin supercharger/$(PRJNAME)_ntsc.bin:
	make -C supercharger mp3

run: $(DBGDIR)/$(PRJNAME)_pal_2k.bin
	$(LS) $<
	$(EMU) -type cv -format pal $<

runntsc: $(DBGDIR)/$(PRJNAME)_ntsc_2k.bin
	$(LS) $<
	$(EMU) -type cv -format ntsc $<

hd: $(DBGDIR)/$(PRJNAME)_pal.bin
	$(HEXDUMP) $<

clean:
	$(RM) $(OBJDIR) $(DBGDIR)
	make -C formenu $@
	make -C menu $@
	make -C supercharger $@
	make -C tools $@

$(OBJDIR) $(DBGDIR):
	mkdir -p $(OBJDIR) $(DBGDIR)

$(DBGDIR)/$(PRJNAME)_pal.bin: $(OBJDIR)/main_pal.o65
	$(LD) -Catari2600_cv_fe.ld -o $@ $^ -m $(DBGDIR)/$(notdir $(basename $@)).map -Ln $(DBGDIR)/$(notdir $(basename $@)).labels -vm

$(DBGDIR)/$(PRJNAME)_ntsc.bin: $(OBJDIR)/main_ntsc.o65
	$(LD) -Catari2600_cv_fe.ld -o $@ $^ -m $(DBGDIR)/$(notdir $(basename $@)).map -Ln $(DBGDIR)/$(notdir $(basename $@)).labels -vm

$(DBGDIR)/$(PRJNAME)_pal_2k.bin: $(DBGDIR)/$(PRJNAME)_pal.bin
	$(CAT) $< $< $< $< >$@

$(DBGDIR)/$(PRJNAME)_ntsc_2k.bin: $(DBGDIR)/$(PRJNAME)_ntsc.bin
	$(CAT) $< $< $< $< >$@

$(OBJDIR)/main_pal.o65: main.s $(OBJDIR) $(wildcard *.inc)
	$(AS) $(ASFLAGS) -DNTSC=0 -o $@ $<

$(OBJDIR)/main_ntsc.o65: main.s $(OBJDIR) $(wildcard *.inc)
	$(AS) $(ASFLAGS) -DNTSC=1 -o $@ $<


