
RODATA_SEGMENT

songdata:
   .byte $2b,$11,$13,$1f,$2b,$11,$13,$1f
   .byte $1b,$0e,$0f,$0e,$1f,$0f,$11,$0f
; data for base tones
   .byte $37,$37,$32,$34

CODE_SEGMENT

song:
   lda   frmcnt
:
   lsr
   lsr
   lsr
   lsr
   tay
   lsr
   lsr
   tax
   lda   songdata,y
   sta   AUDF0
   lsr
   lsr
   and   #$08
   ora   #$04
   sta   AUDC0
   lda   songdata+$10,x
   sta   AUDF1
   lda   #$0c
   sta   AUDC1
   lda   revcnt
   asl
   sta   AUDV0
   lsr   ;lda   revcnt
   lsr
   lsr
   and   #$07
   sta   AUDV1
