
.ifndef NTSC
.define NTSC 0
.endif

.if 0
; best so far
SIN8XSI  := $04
SIN8YSI  := $03
SIN16XSI := $01d5
SIN16YSI := $01c9
SIN8XI   := $01
SIN8YI   := $03
SIN16XI  := $04
SIN16YI  := $09
.else
SIN8XSI  := $02
SIN8YSI  := $01
SIN16XSI := $01c9
SIN16YSI := $00d5
SIN8XI   := $01
SIN8YI   := $03
SIN16XI  := $04
SIN16YI  := $07
.endif

; timer values for vblank and overscan are for TIM64TI, screen is for TIM1KTI
; PAL values, determined by trial an error
; values are calculated by 2*timer value | timer index (0: TIM1KTI, 1: TIM64TI)
.if NTSC
.define TIMER_SCREEN   0+2*$12
.define TIMER_OVERSCAN 1+2*$16 ; ~1280 cycles
.else
.define TIMER_SCREEN   0+2*$16 ; both vblank and screen area
.define TIMER_OVERSCAN 1+2*$0f ; ~1280 cycles
.endif

; all symbols visible to other source files

siner := $f000
colr  := $f100
coder := $f200

colw  := colr   | $400
sinew := siner  | $400
codew := coder  | $400

; 0pglobals.s
.globalzp frmcnt
.globalzp revcnt
.globalzp tcolst
.globalzp tcol
.globalzp tmp8
.globalzp coderam

; main.s
.global   reset
.global   waitvblank   ; called with jsr; x,y untouched
.global   waitscreen   ; called with jsr; x,y untouched
.global   waitoverscan ; called with jmp
; alternative versions, that take use A instead of default for delay
.global   waitvblanka
.global   waitscreena
.global   waitoverscana

; gentables.s
.global   gentables

; payload.s
.global   payload

; song.s
.global   song

