
;   ldx   #$00
@colloop:
   txa
.if NTSC
   and   #$f0
   sta   tmp8
.else
   lsr
   lsr
   lsr
   lsr
   tay
.endif
   txa
   and   #$0f
   cmp   #$08
   bcc   :+
   eor   #$0f
:
   asl
.if NTSC
   ora   tmp8
.else
   ora   highs,y
.endif
   sta   colw,x

   inx
   bne   @colloop

; sine table

   ldy   #$3f

; Accumulate the delta (normal 16-bit addition)
@sinloop:
   clc
   lda   delta+0
   adc   value+0
   sta   value+0
   lda   delta+1
   adc   value+1
   sta   value+1

; Reflect the value around for a sine wave
   sta   sinew+$c0,x
   sta   sinew+$80,y
   eor   #$7f
   sta   sinew+$40,x
   sta   sinew+$00,y

; Increase the delta, which creates the "acceleration" for a parabola
   lda   #$08
   adc   delta+0   ; this value adds up to the proper amplitude
   sta   delta+0
   bcc   :+
   inc   delta+1
:
   inx
   dey
   bpl   @sinloop

   ldx   #$17
   iny   ;ldy   #$00
   lda   #$a8    ; !tay! ($aa:!tax!)
   .byte $2c     ; skip first txa
@codeloop:
   lda   #$98    ; ($3a:!txa)
   sta   codew,y ; !tya!
   iny
   lda   #$65
   sta   codew,y ; !adc! $zp
   iny
   txa
   clc
   adc   #plasmax
   sta   codew,y ; adc !$zp!
   iny
   txa
   and   #$08
   asl
   asl
   asl
   eor   #$66    ; select ror or rol
   sta   codew,y ; !ror! $zp
   iny
   txa
   and   #$18
   lsr
   lsr
   lsr
   adc   #bm0
   sta   codew,y ; ror !$zp!
   iny
   dex
   bpl   @codeloop
   lda   #$60
   sta   codew,y ; !rts!
