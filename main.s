
.include "vcs.inc"
.include "globals.inc"

.ifndef ARSC
.define ARSC 0
.endif

.if ARSC
.define RODATA_SEGMENT .segment "RODATA3"
.define CODE_SEGMENT   .segment "CODE3"
.else
.define RODATA_SEGMENT .segment "RODATA"
.define CODE_SEGMENT   .segment "CODE"
.segment "VECTORS"
; nmi removed, because it will never happen on an 6507
.addr reset ; RESET
.addr waitoverscan + 8 ; IRQ: will only occur with brk and crash
.endif

.macro RPT rpt, cmd
.repeat rpt, I
   cmd
.endrep
.endmacro

.macro ADD8 addr, _a8
   clc               ; 2= 2
   lda   addr        ; 3= 5
   adc   #_a8        ; 2= 7
   sta   addr        ; 3=10
.endmacro

.macro ADD16 addr, _a16
   clc               ; 2= 2
   lda   addr+1      ; 3= 5
   adc   #<_a16      ; 2= 7
   sta   addr+1      ; 3=10
   lda   addr+0      ; 3=13
   adc   #>_a16      ; 2=15
   sta   addr+0      ; 3=18
.endmacro


.segment "ZEROPAGE"

.if NTSC
PLASMAY := $33
.else
PLASMAY := $40
.endif

plasmay:
   .res  PLASMAY
plasmax:
   .res  $18
plasmaend:

frmcnt:
   .byte 0
revcnt:
   .byte 0

bm0:
   .byte 0
bm1:
   .byte 0
bm2:
   .byte 0

tmp8:
   .byte $0
value:
   .word $0000
delta:
   .word $0000

cntx:
   .byte $00
cnty:
   .byte $00
sin8x:
   .byte $00
sin8xs:
   .byte $00
sin8y:
   .byte $00
sin8ys:
   .byte $00
sin16x:
   .word $0000
sin16xs:
   .word $0000
sin16y:
   .word $0000
sin16ys:
   .word $0000
colsw:
   .byte $00
colinit:
   .byte $00
sin16ysi:
   .word $00

RODATA_SEGMENT

.if NTSC
.else
highs:
   .byte $00,$20,$20,$40,$60,$80,$A0,$C0
   .byte $D0,$B0,$90,$70,$50,$30,$30,$20
.endif

CODE_SEGMENT

waitvblank:
   lda   #TIMER_SCREEN
   .byte $2c
waitscreen:
   lda   #TIMER_OVERSCAN
waitvblanka:
waitscreena:
@waitloop:
   bit   TIMINT
   bpl   @waitloop
   lsr
   sta   WSYNC
   sta   TIM64TI
   bcs   @no1k
   sta   TIM1KTI
@no1k:
   rol
   asl
   sta   VBLANK
   rts

reset:
   cld
   lda   #$00
:
   tsx
   pha
   bne   :-          ; SP=$FF, X = A = 0

.if ARSC
.include "gentables_arsc.inc"
.else
.include "gentables.inc"
.endif

   lda   #$0e
   sta   colsw

waitoverscan:
   lda   #TIMER_SCREEN
waitoverscana:
   jsr   waitscreena

   lda   #%00001110  ; each '1' bits generate a VSYNC ON line (bits 1..3)
@syncloop:
   sta   WSYNC
   sta   VSYNC       ; 1st '0' bit resets VSYNC, 2nd '0' bit exit loop
   lsr
   bne   @syncloop   ; branch until VSYNC has been reset

; vblank area

   sta   RESP0
   lda   #$01
   sta   CTRLPF
   lda   #$0d
   sta   NUSIZ0
   sta   NUSIZ1
   sta   REFP0
   
   lda   #$10
   sta   HMP1
   lda   #$40
   sta   HMP0

   tsx
   stx   tmp8
   ldx   #(<plasmaend-1)   ; start writing at $00d7 downto $0080
   txs

   lda   #PLASMAY
   sta   cnty
   lda   #$18
   sta   cntx

   ldx   sin8xs
   ldy   sin16xs

   dec   revcnt

   sta   RESP1

   sta   WSYNC
   sta   HMOVE

@loopX:
   clc
   lda   siner,x
   adc   siner,y
   pha
   RPT   SIN8XI, dex
   tya
   adc   #SIN16XI
   tay
   dec   cntx
   bne   @loopX

   ldx   sin8ys
   ldy   sin16ys
@loopY:
   clc
   lda   siner,x
   adc   siner,y
   pha
   RPT   SIN8YI, dex
   tya
   adc   #SIN16YI
   tay
   dec   cnty
   bne   @loopY

   ldx   tmp8              ; can be hardcoded
   txs

; screen area

   ldx   #(PLASMAY-1)
@disploop:
   lda   plasmay,x
   jsr   coder

   lda   colsw
   bne   :+
   txa
   adc   frmcnt
   tay
   lda   colr,y

:
   sta   WSYNC
   sta   COLUBK
   lda   bm2
   sta   GRP0
   sta   GRP1
   lda   bm1
   sta   PF1
   lda   bm0
   sta   PF2
   dex
   bpl   @disploop

   jsr   waitscreen

.include "song.inc"

   ADD16 sin16xs, SIN16XSI
;   ADD16 sin16ys, SIN16YSI
   ADD8  sin8xs,  SIN8XSI
   ADD8  sin8ys,  SIN8YSI

   lda   #$f0
   sta   sin16ysi+0
   ldx   frmcnt
   lda   siner,x
   lsr
   ror   sin16ysi+0
   lsr
   ror   sin16ysi+0
   lsr
   ror   sin16ysi+0
   lsr
   ror   sin16ysi+0
   lsr
   ror   sin16ysi+0
   sta   sin16ysi+1

   clc
   lda   sin16ys+1
   adc   sin16ysi+0
   sta   sin16ys+1
   lda   sin16ys+0
   adc   sin16ysi+1
   sta   sin16ys+0

   ldy   #$ff
   sty   GRP0
   sty   GRP1
   sty   PF1
   sty   PF2
   inc   frmcnt
   bne   @forcebw
   sty   colinit
@forcebw:

   ldx   #$00
   lda   SWCHB
   and   colinit
   and   #$08
   bne   :+
   ldx   #$0e
:
   stx   colsw

   jmp   waitoverscan
